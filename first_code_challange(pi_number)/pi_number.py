#!/usr/bin/env python3
#-*-coding:cp1252-*-
import sys


def main():
    print(" ", sys.argv)

    pi = 3
    n = 2
    sign = 1

    # solution with iteration
    for i in range(1000001):
        # sign = sign * (-1)
        pi = pi + (sign * (4 / ((n) * (n + 1) * (n + 2))))
        sign = sign * (-1)
        n = n + 2

    print(f"the pi number is {pi}")


if __name__ == "__main__":
    main()
