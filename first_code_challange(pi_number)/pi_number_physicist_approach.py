#!/usr/bin/env python3
#-*-coding:cp1252-*-
import sys
import random
import numpy as np
import matplotlib.pyplot as plt
import time


def main():
    print(" ", sys.argv)

    ntotal = 0
    n1 = 0
    # xtemp = 0
    # ytemp = 0

    # plt.ion()
    #fig = plt.subplots()

    #plt.style.use('seaborn')
    plt.plot([0, 1, 2])

    # fig = plt.figure()
    # ax = fig.add_subplot(111)
    # ax.plot([0, 2, 5, 8, 10, 1, 3, 14], 'ro-')
    # ax.margins(x=0, y=0)
    # grid_points = [0, 1, 2, 3]
    # ax.xaxis.set_ticks(grid_points)

    # in order to approach 3.14 it needs a lot of iterations
    # that is why we need at least 1000000 times to produce random points
    while ntotal < 10000:
        xtemp = random.random()
        ytemp = random.random()
        # r = vector(xtemp, ytemp, 0)
        r = (xtemp, ytemp)

        """
        Calculates the magnitude (or length) of a vector.
        A vector is a direction in space commonly used
        in computer graphics and linear algebra.
        Because it has no "start" position,
        the magnitude of a vector can be thought of as the distance
        from coordinate (0,0) to its (x,y) value.
        Therefore, mag() is a shortcut for writing dist(0, 0, x, y).
        """
        x = np.array(r)
        # np.linalg.norm(x)
        mag = np.sqrt(x.dot(x))
        if mag < 1:
            n1 = n1 + 1
            # plt.xlim(0, mag)
            # plt.ylim(0, mag)
            # plt.grid()
            # plt.plot(xtemp, ytemp, marker="o", markersize=15,
            # markeredgecolor = "green", markerfacecolor = "green")
            # plt.show()
            # print(mag)
            #plt.scatter(0, mag, s=100, c='green')
            plt.scatter(xtemp, ytemp, s=100, c='green')

        else:
            #plt.xlim(0, mag)
            #plt.ylim(0, mag)
            # plt.grid()
            # plt.plot(xtemp, ytemp, marker = "o", markersize = 15,
            #         markeredgecolor = "blue", markerfacecolor = "blue")
            # plt.show()
            # print(mag)
            #plt.scatter(0, mag, s=100, c='blue')
            plt.scatter(xtemp, ytemp, s=100, c='blue')
        # plt.show()
        # plt.pause(0.5)
        # plt.draw()
        ntotal += 1

    # plt.grid()
    # plt.plot(xtemp, ytemp, marker="o", markersize=20, markeredgecolor="red", markerfacecolor="green")
    plt.show()
    pi = 4 * n1 / ntotal
    print(f"pi is about: {pi}")


if __name__ == "__main__":
    main()
