#!/usr/bin/env python3
#-*-coding:cp1252-*-
import sys


def main():
    print(" ", sys.argv)

    pi = 3
    n = 2
    sign = 1

    # solution with recursion

    def pi_rec(i: int, pi: float, n: int, sign):
        if i < 1:
            return pi
        else:
            pi = pi + (sign * (4 / ((n) * (n + 1) * (n + 2))))
            sign = sign * (-1)
            return pi_rec(i - 1, pi, n + 2, sign)

    result_pi = pi_rec(100, pi, n, sign)
    print(f"the pi number is {result_pi}")


if __name__ == "__main__":
    main()
