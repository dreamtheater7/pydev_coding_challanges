#!/usr/bin/env python3
#-*-coding:cp1252-*-
import sys


def main():
    print(" ", sys.argv)

    pi = 3
    n = 2
    sign = 1

    # PI = 4 * N * (r <= 1) / N
    # N are tuples of two random numbers between 0 and 1
    # and r = sqrt(x^2 + y^2) x and y are random numbers

    # pi = 4 *
    nums = tuple(random.random() for _ in range(2))
    x = nums[0] * nums[0]
    y = nums[1] * nums[1]
    num = (x + y)**(1 / 2)
    print(num)
    res = (4 * num) / num
    # print(res)

    def T(n):
        # using some simple math for arithmetic progressions
        Tn = (4 / (2 * n - 1) * (-1)**(1 - n))
        if n == 0:
            return 0
        if n == 1:
            return 4
        else:
            return Tn + T(n - 1)

    result_p = T(990)
    print(result_p)


if __name__ == "__main__":
    main()
